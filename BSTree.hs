
{-# LANGUAGE OverloadedStrings #-}

module BSTree( 
  BSTree(..), insert, delete, 
  find, height, width, inOrder, invert
) where


import Data.Monoid 
import qualified Data.Text as T(Text, pack)
import Prelude hiding (min)

data BSTree = Empty | Node Int BSTree BSTree 
  deriving Show

insert :: Int -> BSTree -> BSTree
insert key Empty = Node key Empty Empty
insert key (Node x left right) 
  | key < x = Node x (insert key left) right
  | otherwise = Node x left (insert key right)

delete :: Int -> BSTree -> BSTree
delete key Empty = Empty
delete key (Node x left right)
  | key == x = delete' (Node x left right)
  | key < x = Node x (delete key left) right
  | otherwise = Node x left (delete key right)

delete' :: BSTree -> BSTree
delete' (Node x Empty Empty) = Empty
delete' (Node x left Empty) = left
delete' (Node x Empty right) = right
delete' (Node x left right) = Node succesor left (delete succesor right)
  where succesor = min right

min :: BSTree -> Int
min (Node x Empty _) = x
min (Node x left _) = min left

find :: Int -> BSTree -> Maybe Int
find key Empty = Nothing
find key (Node x left right) 
  | key == x = Just x
  | key < x = find key left
  | otherwise = find key right

height :: BSTree -> Int
height Empty = -1
height (Node x left right) = 1 + maximum [(height left), (height right)]

width :: BSTree -> Int
width tree = maximum $ map (levelWidth tree) [1..(height tree)]

levelWidth :: BSTree -> Int -> Int
levelWidth Empty _ = 0
levelWidth (Node x left right) level
  | level == 1 = 1
  | level > 1 = levelWidth left (level - 1) + levelWidth right (level - 1)
  | otherwise = -1 {-should never happen-}

invert :: BSTree -> BSTree 
invert Empty = Empty
invert (Node x l r) = Node x (invert r) (invert l)

inOrder :: BSTree -> T.Text
inOrder Empty = ""
inOrder (Node x left right) = (inOrder left) <> T.pack (show x) <> " " <> (inOrder right) 
