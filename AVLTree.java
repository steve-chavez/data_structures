
import java.util.*;

public class AVLTree{

  private class Node{
    Integer key;
    Integer balancingFactor; 
    Optional<Node> left = Optional.empty(), right = Optional.empty();

    public Node(Integer key, Integer bf){ this.key = key; this.balancingFactor = bf; }
  }

  private class Rel{
    Optional<Node> parent;
    Optional<Node> node;
    Optional<Boolean> isLeft;

    public Rel(Optional<Node> parent, Optional<Node> node, 
               Optional<Boolean> isLeft){ 
      this.parent = parent; this.node = node; this.isLeft = isLeft; 
    }
  }

  private Optional<Node> root = Optional.empty();

  public void insert(Integer key){
    Optional<Node> n = Optional.of(new Node(key, 0));
    if( !this.root.isPresent() ){
      this.root = n;
    }else{
      Optional<Node> cur = this.root;
      // This stack could be replaced by the stack of a recursive method, 
      // this is just to show the iterative form.
      Stack<Rel> nodes = new Stack<Rel>();
      nodes.push(new Rel(Optional.empty(), cur, Optional.of(Boolean.TRUE)));
      while( cur.isPresent() ){
        if( key < cur.get().key )
          if( cur.get().left.isPresent() ){
            nodes.push(new Rel(cur, cur.get().left, Optional.of(Boolean.TRUE)));
            cur = cur.get().left;
          }else{ 
            cur.get().left = n; 
            break; 
          }
        else if( key > cur.get().key )
          if( cur.get().right.isPresent() ){
            nodes.push(new Rel(cur, cur.get().right, Optional.of(Boolean.FALSE)));
            cur = cur.get().right;
          }else{ 
            cur.get().right = n; 
            break; 
          }
        else
          return;
      }
      this.updateBalances(nodes);
      this.rotateUnbalanced(nodes);
      this.updateBalances(nodes);
    }
  }

  private void updateBalances(Stack<Rel> nodes){
    while(!nodes.empty()){
      Node n = nodes.pop().node.get();
      n.balancingFactor = treeHeight(n.right) - treeHeight(n.left);
    }
  }

  private void rotateUnbalanced(Stack<Rel> nodes){
    while(!nodes.empty()){
      Rel r = nodes.pop();
      Node n = r.node.get();
      if(n.balancingFactor < -1){
        Node child = n.left.get();
        if(child.balancingFactor == -1) //left left
          rotateRight(r);
        if(child.balancingFactor == 1){ //left right
          rotateLeft(new Rel(r.node, Optional.of(child), Optional.of(Boolean.TRUE)));
          rotateRight(r);
        }
        break;
      }else if(n.balancingFactor > 1){
        Node child = n.right.get();
        if(child.balancingFactor == 1) //right right
          rotateLeft(r);
        if(child.balancingFactor == -1){ //right left
          rotateRight(new Rel(r.node, Optional.of(child), Optional.of(Boolean.FALSE)));
          rotateLeft(r);
        }
        break;
      }
    }
  }

  //
  //      z         y
  //     /         / \
  //    y    ---> x   z
  //   /
  //  x
  private void rotateRight(Rel r){
    Optional<Node> z = r.node;
    Optional<Node> y = z.get().left;
    Optional<Node> t3 = y.get().right;

    y.get().right = z;
    z.get().left = t3;

    if(r.parent.isPresent()){
      if(r.isLeft.get())
        r.parent.get().left = y;
      else
        r.parent.get().right = y;
    } else
      this.root = y;
  }

  //
  //  x             y
  //   \           / \
  //    y    ---> x   z
  //     \ 
  //      z 
  private void rotateLeft(Rel r){
    Optional<Node> x = r.node;
    Optional<Node> y = x.get().right;
    Optional<Node> t2 = y.get().left;

    y.get().left = x;
    x.get().right = t2;

    if(r.parent.isPresent()){
      if(r.isLeft.get())
        r.parent.get().left = y;
      else
        r.parent.get().right = y;
    } else
      this.root = y;

  }

  public boolean isAVL(){
    return this.isAVL(this.root);
  }

  private boolean isAVL(Optional<Node> n){
    if(n.isPresent()){
      return isAVL(n.get().left) && 
             (n.get().balancingFactor >= -1 || n.get().balancingFactor <= 1) &&
             isAVL(n.get().right);
    } else return true;
  }

  private Integer treeHeight(Optional<Node> n){
    if(!n.isPresent())
      return -1;
    else
      return 1 + Math.max(treeHeight(n.get().left), 
                          treeHeight(n.get().right)); 
  }

  private String inOrder(Optional<Node> n){
    if(n.isPresent()){
      return inOrder(n.get().left) + n.get().key + " " + inOrder(n.get().right);
    } else return "";
  }

  @Override
  public String toString(){
    return inOrder(this.root).trim();
  }
}
