
import static com.mscharhag.oleaster.runner.StaticRunnerSupport.*;
import com.mscharhag.oleaster.runner.OleasterRunner;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

@RunWith(OleasterRunner.class)
public class BSTreeTest {{
  describe("BSTree class", () -> {
    it("should insert keys", () -> {
      BSTree t = new BSTree();
      t.insert(3); t.insert(5); t.insert(9); t.insert(10); t.insert(1);
      t.insert(4);
      assertEquals(t.toString(), "1 3 4 5 9 10");
    });

    it("shouldn't delete if key is not present", () -> {
      BSTree t = new BSTree();
      t.insert(3); t.insert(5); t.insert(9); t.insert(10); t.insert(1);
      t.insert(4);
      t.delete(12); t.delete(100); t.delete(2);
      assertEquals(t.toString(), "1 3 4 5 9 10");
    });

    it("should find key if it's present", () -> {
      BSTree t = new BSTree();
      t.insert(3); t.insert(5); t.insert(9); t.insert(10); t.insert(1);
      t.insert(4);
      assertTrue(t.find(3));
    });

    it("shouldn't find key if it's not present", () -> {
      BSTree t = new BSTree();
      t.insert(3); t.insert(5); t.insert(9); t.insert(10); t.insert(1);
      t.insert(4);
      assertFalse(t.find(8));
    });

    describe("root node deletion", () -> {
      it("of a tree with just root node", () -> {
        BSTree t = new BSTree();
        t.insert(3);
        t.delete(3);
        assertEquals(t.toString(), "");
      });
      it("of a tree with a root node with a leaf", () -> {
        BSTree t1 = new BSTree();
        t1.insert(3); t1.insert(5);
        t1.delete(3);
        assertEquals(t1.toString(), "5");
        BSTree t2 = new BSTree();
        t2.insert(3); t2.insert(1);
        t2.delete(3);
        assertEquals(t2.toString(), "1");
      });
      it("of a tree with a root node and two leaves", () -> {
        BSTree t1 = new BSTree();
        t1.insert(3); t1.insert(5); t1.insert(2);
        t1.delete(3);
        assertEquals(t1.toString(), "2 5");
      });
    });

    describe("non-root node deletion", () -> {
      it("of a leaf", () -> {
        BSTree t = new BSTree();
        t.insert(38); t.insert(26); t.insert(72); t.insert(55); t.insert(90);
        t.insert(41); t.insert(60); t.insert(43); t.insert(78); t.insert(92);
        t.insert(74);
        t.delete(43);
        assertEquals(t.toString(), "26 38 41 55 60 72 74 78 90 92");
        t.delete(74);
        assertEquals(t.toString(), "26 38 41 55 60 72 78 90 92");
        t.delete(92);
        assertEquals(t.toString(), "26 38 41 55 60 72 78 90");
        t.delete(26);
        assertEquals(t.toString(), "38 41 55 60 72 78 90");
      });
      describe("of a node with one child", () -> {
        BSTree t = new BSTree();
        t.insert(38); t.insert(26); t.insert(72); t.insert(55); t.insert(90);
        t.insert(41); t.insert(60); t.insert(43); t.insert(78); t.insert(92);
        t.insert(74);
        it("a left child", () -> {
          t.delete(78);
          assertEquals(t.toString(), "26 38 41 43 55 60 72 74 90 92");
        });
        it("a right child", () -> {
          t.delete(41);
          assertEquals(t.toString(), "26 38 43 55 60 72 74 90 92");
        });
      });
      describe("of a node with two childrens", () -> {
        it("and the succesor has no right child", () -> {
          BSTree t = new BSTree();
          t.insert(38); t.insert(26); t.insert(72); t.insert(55); t.insert(90);
          t.insert(41); t.insert(60); t.insert(43); t.insert(78); t.insert(92);
          t.insert(74);
          t.delete(72);
          assertEquals(t.toString(), "26 38 41 43 55 60 74 78 90 92");
        });
        it("and the succesor has a right child", () -> {
          BSTree t = new BSTree();
          t.insert(38); t.insert(26); t.insert(72); t.insert(55); t.insert(90);
          t.insert(41); t.insert(60); t.insert(43); t.insert(78); t.insert(92);
          t.insert(74);
          t.delete(38);
          assertEquals(t.toString(), "26 41 43 55 60 72 74 78 90 92");
        });
      });
    });

    it("should calculate the correct height", () -> {
      BSTree t = new BSTree();
      t.insert(38); t.insert(26); t.insert(72); t.insert(55); t.insert(90);
      t.insert(41); t.insert(60); t.insert(43); t.insert(78); t.insert(92);
      t.insert(74);
      assertTrue(t.height() == 4);
      BSTree t2 = new BSTree();
      t2.insert(38); 
      assertTrue(t2.height() == 0);
    });

    it("should calculate the correct width", () -> {
      BSTree t = new BSTree();
      t.insert(38); t.insert(26); t.insert(72); t.insert(55); t.insert(90);
      t.insert(41); t.insert(60); t.insert(43); t.insert(78); t.insert(92);
      t.insert(74);
      assertTrue(t.width() == 4);
    });

    it("should invert the tree", () -> {
      BSTree t = new BSTree();
      t.insert(4); t.insert(2); t.insert(1); t.insert(3); t.insert(7);
      t.insert(6); t.insert(9);
      assertEquals(t.toString(), "1 2 3 4 6 7 9");
      t.invert();
      assertEquals(t.toString(), "9 7 6 4 3 2 1");
      t.invert();
      assertEquals(t.toString(), "1 2 3 4 6 7 9");
    });
  });
}}
