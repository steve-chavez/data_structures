
import static com.mscharhag.oleaster.runner.StaticRunnerSupport.*;
import com.mscharhag.oleaster.runner.OleasterRunner;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;
import java.util.Random;

@RunWith(OleasterRunner.class)
public class AVLTreeTest {{
  describe("AVLTree", () -> {
    it("should insert keys and maintain AVL property", () -> {
      AVLTree t = new AVLTree();
      t.insert(38); t.insert(26); t.insert(72); t.insert(55); t.insert(90);
      t.insert(41); t.insert(60); t.insert(43); t.insert(78); t.insert(92);
      t.insert(74);
      assertEquals(t.toString(), "26 38 41 43 55 60 72 74 78 90 92");
      assertTrue(t.isAVL());
    });

    it("should insert 100 random keys and maintain AVL property", () -> {
      AVLTree t = new AVLTree();
      Random ran = new Random();
      for(int i=0; i < 100; i++){
        int x = ran.nextInt(100);
        t.insert(x);
      }
      assertTrue(t.isAVL());
    });

  });
}}
