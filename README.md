
# Data structures practice

For how to run the tests see `build.sh`.

## Pending

- Understand why 100 random keys from 1 to 100 maintain a red black tree
  of 4 black height max.
- Deletion in AVL and RB trees.
- Ask why RBTree impl's in haskell are so complicated https://gist.github.com/rampion/2659812
