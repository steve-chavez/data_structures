import static com.mscharhag.oleaster.runner.StaticRunnerSupport.*;
import com.mscharhag.oleaster.runner.OleasterRunner;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;
import java.util.Random;

@RunWith(OleasterRunner.class)
public class WeightedGraphTest {{
  describe("WeightedGraph", () -> {

    // From images/weighted_graph.jpg
    WeightedGraph g1 = new WeightedGraph(9);
    g1.addEdge(0, 1, 4); g1.addEdge(0, 7, 8);
    g1.addEdge(1, 7, 11); g1.addEdge(1, 2, 8);
    g1.addEdge(2, 3, 7); g1.addEdge(2, 8, 2); g1.addEdge(2, 5, 4);
    g1.addEdge(3, 4, 9); g1.addEdge(3, 5, 14);
    g1.addEdge(4, 5, 10); g1.addEdge(3, 5, 14);
    g1.addEdge(5, 6, 2);
    g1.addEdge(6, 7, 1); g1.addEdge(6, 8, 6);
    g1.addEdge(7, 8, 7);

    // From images/weighted_graph_2.png
    WeightedGraph g2 = new WeightedGraph(5);
    g2.addDirectedEdge(0, 1, 50); g2.addDirectedEdge(0, 3, 80);
    g2.addDirectedEdge(1, 2, 60); g2.addDirectedEdge(1, 3, 90);
    g2.addDirectedEdge(2, 4, 40);
    g2.addDirectedEdge(3, 2, 20);
    g2.addDirectedEdge(3, 4, 70);
    g2.addDirectedEdge(4, 1, 50);

    //
    // 0 <--70---1
    // ^         |
    // |         10
    // 30        |
    // |         v
    // 2<--20----3
    WeightedGraph g3 = new WeightedGraph(4);
    g3.addDirectedEdge(1, 0, 70); g3.addDirectedEdge(1, 3, 10);
    g3.addDirectedEdge(2, 0, 30);
    g3.addDirectedEdge(3, 2, 20);

    it("should show the minimum spanning tree", () -> {
      assertEquals(
        g1.minimumSpanningTree(),
        "* 0-1 0-7 7-6 6-5 5-2 2-8 2-3 3-4");
    });

    it("should show shortest path array starting from a given vertex(using dijkstra)", () -> {
      assertEquals(
        g1.dijkstraShortestPath(0),
        new Integer[]{
          0, 4, 12, 19, 21, 11, 9, 8, 14});
    });

    it("should show shortest path adjacency matrix(using floyd-warshall)", () -> {
      assertEquals(
        g3.floydWarshall(),
        new Integer[][]{
          { Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE },
          { 60, Integer.MAX_VALUE, 30, 10 },
          { 30, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE },
          { 50, Integer.MAX_VALUE, 20, Integer.MAX_VALUE }});
    });
  });
}}
