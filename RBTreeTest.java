
import static com.mscharhag.oleaster.runner.StaticRunnerSupport.*;
import com.mscharhag.oleaster.runner.OleasterRunner;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;
import java.util.Random;

@RunWith(OleasterRunner.class)
public class RBTreeTest {{
  describe("RBTree", () -> {
    describe("red uncle case", () -> {
      it("should handle left left subcase", () -> {
        RBTree t = new RBTree();
        t.insert(10); t.insert(20); t.insert(5); t.insert(1);
        assertEquals(t.toString(), "1:red 5:black 10:black 20:black");
        assertTrue(t.blackHeight()==2);
      });

      it("should handle left right subcase", () -> {
        RBTree t = new RBTree();
        t.insert(10); t.insert(20); t.insert(5); t.insert(6);
        assertEquals(t.toString(), "5:black 6:red 10:black 20:black");
        assertTrue(t.blackHeight()==2);
      });

      it("should handle right right subcase", () -> {
        RBTree t = new RBTree();
        t.insert(10); t.insert(20); t.insert(5); t.insert(24);
        assertEquals(t.toString(), "5:black 10:black 20:black 24:red");
        assertTrue(t.blackHeight()==2);
      });

      it("should handle right left subcase", () -> {
        RBTree t = new RBTree();
        t.insert(10); t.insert(20); t.insert(5); t.insert(19);
        assertEquals(t.toString(), "5:black 10:black 19:red 20:black");
        assertTrue(t.blackHeight()==2);
      });
    });

    describe("black uncle case", () -> {
      it("should handle left left subcase", () -> {
        RBTree t = new RBTree();
        t.insert(10); t.insert(5); t.insert(1); 
        assertEquals(t.toString(), "1:red 5:black 10:red");
        assertTrue(t.blackHeight()==1);
      });

      it("should handle right right subcase", () -> {
        RBTree t = new RBTree();
        t.insert(10); t.insert(20); t.insert(30); 
        assertEquals(t.toString(), "10:red 20:black 30:red");
        assertTrue(t.blackHeight()==1);
      });

      it("should handle left right subcase", () -> {
        RBTree t = new RBTree();
        t.insert(10); t.insert(5); t.insert(6); 
        assertEquals(t.toString(), "5:red 6:black 10:red");
        assertTrue(t.blackHeight()==1);
      });
      
      it("should handle right left subcase", () -> {
        RBTree t = new RBTree();
        t.insert(10); t.insert(20); t.insert(15); 
        assertEquals(t.toString(), "10:red 15:black 20:red");
        assertTrue(t.blackHeight()==1);
      });
    });

    it("should handle red uncle then black uncle case", () -> {
        RBTree t = new RBTree();
        t.insert(10); t.insert(20); t.insert(30); t.insert(15); 
        assertEquals(t.toString(), "10:black 15:red 20:black 30:black");
        assertTrue(t.blackHeight()==2);
    });

    it("should handle more complex cases", () -> {
      RBTree t = new RBTree();
      t.insert(2); t.insert(1); t.insert(4); t.insert(5); 
      t.insert(9); t.insert(3); t.insert(6); t.insert(7); 
      //taken from https://www.csee.umbc.edu/courses/undergraduate/341/spring04/hood/notes/red_black/
      assertEquals(t.toString(), 
        "1:black 2:black 3:red 4:black 5:red 6:red 7:black 9:red");
      assertTrue(t.blackHeight()==2);

      RBTree t2 = new RBTree();
      t2.insert(1); t2.insert(2); t2.insert(3); t2.insert(4); t2.insert(5);
      t2.insert(6); t2.insert(7); t2.insert(8); t2.insert(9); t2.insert(10);
      t2.insert(11);
      //taken from https://www.cs.utexas.edu/~scottm/cs314/handouts/slides/Topic23RedBlackTrees.pdf
      assertEquals(t2.toString(), 
        "1:black 2:black 3:black 4:black 5:black 6:black 7:black 8:red 9:red 10:black 11:red");
      assertTrue(t2.blackHeight()==3);
    });

    it("should insert 100 random keys and maintain red black property", () -> {
      RBTree t = new RBTree();
      Random ran = new Random();
      for(int i=0; i < 100; i++){
        int x = ran.nextInt(100);
        t.insert(x);
      }
      assertTrue(t.blackHeight()!=-1);
    });

  });
}}
