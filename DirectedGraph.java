import java.util.*;
import java.util.function.*;

public class DirectedGraph{

  private Integer numVertices;
  //We'll have Integer labeled vertices for easier identification of these on the adjacencyMatrix,
  //row 4 corresponds to label 4 and so on
  private Integer[] vertices;
  private Boolean[][] adjacencyMatrix;

  public DirectedGraph(Integer nVertices){
    this.numVertices = nVertices;
    this.vertices = new Integer[this.numVertices];
    for(int i = 0; i < this.numVertices; i++)
      this.vertices[i] = i;
    this.adjacencyMatrix = new Boolean[this.numVertices][this.numVertices];
    for(int i = 0; i < this.numVertices; i++)
      for(int j = 0; j < this.numVertices; j++)
        this.adjacencyMatrix[i][j] = false;
  }

  // TODO Validate that the labels don't surpass the max
  public void addEdge(Integer v1, Integer v2){
    this.adjacencyMatrix[v1][v2] = true;
  }

  public Optional<String> topologicalOrder(){
    String order = "*";
    // Using marked vertices(visited) to avoid deleting the graph
    Boolean[] markedVertices = new Boolean[this.numVertices];
    for(int i = 0; i < this.numVertices; i++)
      markedVertices[i] = false;
    Stack<Integer> stack = new Stack<Integer>();
    Integer numVerticesTemp = this.numVertices;

    Supplier<Optional<Integer>> findVertexWNoSuccessors = () -> {
      for(int i = 0; i < this.numVertices; i++){
        if(markedVertices[i]) continue;
        for(int j = 0; j < this.numVertices; j++){
          if(this.adjacencyMatrix[i][j] && !markedVertices[j])
            break;
          // If we're at the last iteration and didn't break then we have a match
          if (j == this.numVertices - 1)
            return Optional.of(this.vertices[i]);
        }
      }
      return Optional.empty();
    };

    while(numVerticesTemp > 0){
      Optional<Integer> vertex = findVertexWNoSuccessors.get();
      if(vertex.isPresent()){
        Integer v = vertex.get();
        stack.push(v);
        markedVertices[v] = true;
      } else return Optional.empty();
      numVerticesTemp--;
    }

    while(!stack.empty())
      order += "-" + stack.pop();

    return Optional.of(order);
  }

  // Uses the Warshall algo
  // https://www.geeksforgeeks.org/transitive-closure-of-a-graph/
  public Boolean[][] transitiveClosure(){
    Boolean[][] result = this.adjacencyMatrix.clone();
    for(int row = 0; row < this.numVertices; row++)
      for(int column = 0; column < this.numVertices; column++)
        if(result[row][column])
          for(int x = 0; x < this.numVertices; x++)
            if(result[x][row])
              result[x][column] = true;
    return result;
  }
}
