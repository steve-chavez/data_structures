
import static com.mscharhag.oleaster.runner.StaticRunnerSupport.*;
import com.mscharhag.oleaster.runner.OleasterRunner;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;
import java.util.Random;

@RunWith(OleasterRunner.class)
public class DirectedGraphTest {{
  describe("Topological sorting", () -> {

    //  0 - 3
    //    \   \
    //  1 - 4 - 6
    //            \
    //  2 ----- 5 - 7
    //
    DirectedGraph g1 = new DirectedGraph(8);
    g1.addEdge(0, 3); g1.addEdge(0, 4);
    g1.addEdge(1, 4);
    g1.addEdge(2, 5);
    g1.addEdge(3, 6);
    g1.addEdge(4, 6);
    g1.addEdge(5, 7);
    g1.addEdge(6, 7);

    //     5     4
    //   /   \ /   \
    //  2     0     \
    //   \           \
    //    3 --------> 1
    DirectedGraph g2 = new DirectedGraph(6);
    g2.addEdge(2, 3);
    g2.addEdge(3, 1);
    g2.addEdge(4, 0); g2.addEdge(4, 1);
    g2.addEdge(5, 2); g2.addEdge(5, 0);

    //     1
    //   />  \>
    //  0 <-- 2
    DirectedGraph g3 = new DirectedGraph(3);
    g3.addEdge(0, 1);
    g3.addEdge(1, 2);
    g3.addEdge(2, 0);

    it("should succeed on a DAG", () -> {
      assertEquals(
        g1.topologicalOrder().get(),
        "*-1-0-4-3-6-2-5-7");
      assertEquals(
        g2.topologicalOrder().get(),
        "*-5-4-2-3-1-0");
    });

    it("should fail on a cyclic graph", () -> {
      assertFalse(g3.topologicalOrder().isPresent());
    });

  });

  describe("Warshall algorithm", () -> {
    //   0 -> 1
    //  / ↖   |
    //  ↘ /   |
    //   2 <---
    //   |
    //    \-->3
    DirectedGraph g1 = new DirectedGraph(4);
    g1.addEdge(0, 1); g1.addEdge(0, 2);
    g1.addEdge(1, 2);
    g1.addEdge(2, 0); g1.addEdge(2, 3);

    //   0 -> 1
    //    ↖   |
    //      \ ↓
    //   2 <- 3
    DirectedGraph g2 = new DirectedGraph(4);
    g2.addEdge(0, 1);
    g2.addEdge(1, 3);
    g2.addEdge(3, 0); g2.addEdge(3, 2);

    it("should give the transitive closure", () -> {
      assertEquals(
        g1.transitiveClosure(),
        new Boolean[][]{
          { true, true, true, true },
          { true, true, true, true },
          { true, true, true, true },
          { false, false, false, false }}
      );
      assertEquals(
        g2.transitiveClosure(),
        new Boolean[][]{
          { true, true, true, true },
          { true, true, true, true },
          { false, false, false, false },
          { true, true, true, true }}
      );
    });
  });
}}
