import java.util.*;
import java.util.function.*;

public class Graph{

  private Integer numVertices;
  //We'll have Integer labeled vertices for easier identification of these on the adjacencyMatrix,
  //row 4 corresponds to label 4 and so on
  private Integer[] vertices;
  private Boolean[][] adjacencyMatrix;

  public Graph(Integer nVertices){
    this.numVertices = nVertices;
    this.vertices = new Integer[this.numVertices];
    for(int i = 0; i < this.numVertices; i++)
      this.vertices[i] = i;
    this.adjacencyMatrix = new Boolean[this.numVertices][this.numVertices];
    for(int i = 0; i < this.numVertices; i++)
      for(int j = 0; j < this.numVertices; j++)
        this.adjacencyMatrix[i][j] = false;
  }

  // TODO Validate that both vertices exist
  public void addEdge(Integer v1, Integer v2){
    this.adjacencyMatrix[v1][v2] = true;
    this.adjacencyMatrix[v2][v1] = true;
  }

  // Also called depth first search or DFS, traversal seems more accurate to me
  public String depthFirstTraversal(){
    String visitOrder = "*";
    Boolean[] visitedVertices = new Boolean[numVertices];
    for(int i = 0; i < numVertices; i++)
      visitedVertices[i] = false;
    Stack<Integer> stack = new Stack<Integer>();

    //Visit first vertex
    visitedVertices[0] = true;
    stack.push(this.vertices[0]);
    visitOrder += "-" + stack.peek();

    while(!stack.empty()){
      Function<Integer, Optional<Integer>> findUnvisitedVertex = vertex -> {
        for(int i = 0; i < this.numVertices; i++)
          if(this.adjacencyMatrix[vertex][i] && !visitedVertices[i])
            return Optional.of(this.vertices[i]);
        return Optional.empty();
      };
      Optional<Integer> unvisitedVertex = findUnvisitedVertex.apply(stack.peek());
      if(unvisitedVertex.isPresent()){
        //Visit the next vertex
        visitedVertices[unvisitedVertex.get()] = true;
        stack.push(unvisitedVertex.get());
        visitOrder += "-" + stack.peek();
      }
      else
        stack.pop();
    }

    return visitOrder;
  }

  public String breadthFirstTraversal(){
    String visitOrder = "*";
    Boolean[] visitedVertices = new Boolean[numVertices];
    for(int i = 0; i < numVertices; i++)
      visitedVertices[i] = false;
    Queue<Integer> queue = new LinkedList<Integer>();

    //Visit first vertex
    visitedVertices[0] = true;
    queue.add(this.vertices[0]);
    visitOrder += "-" + queue.peek();

    while(queue.size() > 0){
      Function<Integer, Optional<Integer>> findUnvisitedVertex = vertex -> {
        for(int i = 0; i < this.numVertices; i++)
          if(this.adjacencyMatrix[vertex][i] && !visitedVertices[i])
            return Optional.of(this.vertices[i]);
        return Optional.empty();
      };
      Optional<Integer> unvisitedVertex = findUnvisitedVertex.apply(queue.peek());
      if(unvisitedVertex.isPresent()){
        //Visit the next vertex
        visitedVertices[unvisitedVertex.get()] = true;
        queue.add(unvisitedVertex.get());
        visitOrder += "-" + unvisitedVertex.get();
      }
      else
        queue.remove();
    }

    return visitOrder;
  }

  public String miniumSpanningTree(){
    String visitOrder = "*";
    Boolean[] visitedVertices = new Boolean[numVertices];
    for(int i = 0; i < numVertices; i++)
      visitedVertices[i] = false;
    Stack<Integer> stack = new Stack<Integer>();

    //Visit first vertex
    visitedVertices[0] = true;
    stack.push(this.vertices[0]);

    while(!stack.empty()){
      Function<Integer, Optional<Integer>> findUnvisitedVertex = vertex -> {
        for(int i = 0; i < this.numVertices; i++)
          if(this.adjacencyMatrix[vertex][i] && !visitedVertices[i])
            return Optional.of(this.vertices[i]);
        return Optional.empty();
      };
      Optional<Integer> unvisitedVertex = findUnvisitedVertex.apply(stack.peek());
      if(unvisitedVertex.isPresent()){
        Integer source = stack.peek();
        //Visit the next vertex
        visitedVertices[unvisitedVertex.get()] = true;
        stack.push(unvisitedVertex.get());
        Integer destination = stack.peek();
        visitOrder += " " + source + "-" + destination;
      }
      else
        stack.pop();
    }

    return visitOrder;
  }
}
