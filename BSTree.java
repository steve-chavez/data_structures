
import java.util.*;

public class BSTree{

  private class Node{
    Integer key;
    Optional<Node> left = Optional.empty(), right = Optional.empty();

    public Node(Integer key){ this.key = key; }
  }

  //For finding and modifying node references without adding extra attributes
  //in Node(like parent,..)
  private class Rel{
    Optional<Node> parent;
    Optional<Node> node;
    Optional<Boolean> isLeft;

    public Rel(Optional<Node> parent, Optional<Node> node, 
               Optional<Boolean> isLeft){ 
      this.parent = parent; this.node = node; this.isLeft = isLeft; 
    }
  }

  private Optional<Node> root = Optional.empty();

  public void insert(Integer key){
    Optional<Node> n = Optional.of(new Node(key));
    if( !this.root.isPresent() ) 
      this.root = n;
    else{
      Optional<Node> cur = this.root;
      while( cur.isPresent() ){
        if( key < cur.get().key )
          if( cur.get().left.isPresent() )
            cur = cur.get().left;
          else{ cur.get().left = n; break; }
        else
          if( cur.get().right.isPresent() )
            cur = cur.get().right;
          else{ cur.get().right = n; break; }
      }
    }
  }

  public boolean find(Integer key){
    return findRel(key).node.isPresent();
  }

  private Rel findRel(Integer key){
    Optional<Node> parent = Optional.empty();
    Optional<Node> cur = this.root;
    Optional<Boolean> isLeft = Optional.empty();
    while( cur.isPresent() ){
      if( key == cur.get().key )
        return new Rel(parent, cur, isLeft);
      else {
        parent = cur;
        if( key < cur.get().key ){
          isLeft =  Optional.of(Boolean.TRUE);
          cur = cur.get().left;
        } else{
          isLeft =  Optional.of(Boolean.FALSE);
          cur = cur.get().right;
        }
      }
    }
    return new Rel(Optional.empty(), Optional.empty(), Optional.empty());
  }

  private Rel minRel(Optional<Node> cur){
    Rel rel = new Rel(Optional.empty(), Optional.empty(), Optional.empty());

    //To find parent of given node
    if( cur.isPresent() ){
      rel = findRel(cur.get().key);
    }else
      return rel;

    //Override that rel with min
    while( cur.isPresent() )
      if( cur.get().left.isPresent() ){
        rel.parent = cur;
        cur = cur.get().left;
        rel.node = cur;
        rel.isLeft = Optional.of(Boolean.TRUE);
      } else return rel; 

    return new Rel(Optional.empty(), Optional.empty(), Optional.empty());
  }

  public void delete(Integer key){
    Rel rel = findRel(key);
    Optional<Node> parent = rel.parent;
    Optional<Node> cur = rel.node;
    Optional<Boolean> isLeft = rel.isLeft;
    if( cur.isPresent() ){
      //First if node to delete is a leaf 
      if( !cur.get().left.isPresent() && !cur.get().right.isPresent() )
        if( parent.isPresent() )
          if( isLeft.get() )
            parent.get().left = Optional.empty();
          else
            parent.get().right = Optional.empty();
        else
          this.root = Optional.empty();
      //Second if node to delete has one child
      else if( cur.get().left.isPresent() ^ cur.get().right.isPresent() ){
        Optional<Node> oneChild = cur.get().left.isPresent()?
                                  cur.get().left:cur.get().right;
        if( parent.isPresent() )
          if( isLeft.get() )
            parent.get().left = oneChild;
          else
            parent.get().right = oneChild;
        else
          if( cur.get().left.isPresent() )
            this.root = oneChild;
          else
            this.root = oneChild;
      //Third if node to delete has two childrens
      }else{
        Rel minRel;
        minRel = minRel(cur.get().right);
        cur.get().key = minRel.node.get().key;
        Optional<Node> succesorChild = minRel.node.get().right.isPresent()?
                                       minRel.node.get().right:Optional.empty();
        if( minRel.isLeft.get() )
          minRel.parent.get().left = succesorChild;
        else
          minRel.parent.get().right = succesorChild;
      }
    }
  }

  public Integer height(){
    return treeHeight(this.root);
  }

  private Integer treeHeight(Optional<Node> n){
    if(!n.isPresent())
      return -1;
    else
      return 1 + Math.max(treeHeight(n.get().left), 
                          treeHeight(n.get().right)); 
  }

  public Integer width(){
    Integer maxWidth = 0;
    Integer currentWidth = 0;
    for(int i = 1; i <= this.height(); i++){
      currentWidth = levelWidth(this.root, i);
      if(maxWidth < currentWidth)
        maxWidth = currentWidth;
    }
    return maxWidth;
  }

  private Integer levelWidth(Optional<Node> root, Integer level){
    if(!root.isPresent()) return 0;
    Integer levelsToGo = level;
    if(levelsToGo == 1) return 1; //We are in the desired level
    if(levelsToGo > 1) 
      return levelWidth(root.get().left, levelsToGo - 1) +
             levelWidth(root.get().right, levelsToGo - 1);
    if(level <= 0) throw new IllegalArgumentException();
    return -1; //should never happen
  }

  public void invert(){
    this.root = invertNode(this.root);
  }

  private Optional<Node> invertNode(Optional<Node> n){
    if(!n.isPresent())
      return Optional.empty();
    else{
      Node nn = new Node(n.get().key);
      nn.left = invertNode(n.get().right);
      nn.right = invertNode(n.get().left);
      return Optional.of(nn);
    }
  }
  
  private String inOrder(Optional<Node> n){
    if(n.isPresent()){
      return inOrder(n.get().left) + n.get().key + " " + inOrder(n.get().right);
    } else return "";
  }

  @Override
  public String toString(){
    return inOrder(this.root).trim();
  }
  
}

