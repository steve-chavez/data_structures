{-# LANGUAGE OverloadedStrings #-}

module AVLTree( 
  AVLTree(..), insert, inOrder, isAVL
) where

import Data.Monoid 
import qualified Data.Text as T(Text, pack)
import Prelude

data AVLTree = 
  Empty | Node { 
    key     :: Int, 
    left    :: AVLTree, 
    right   :: AVLTree,
    bFactor :: Int 
  } deriving Show

insert :: Int -> AVLTree -> AVLTree
insert key tree = 
  case tree of
    Empty -> Node key Empty Empty 0
    t@(Node k l r _) 
      | key < k -> rotateUnbalanced $ Node k l' r (height r - height l') 
      | key == k -> t {- Duplicates not allowed -}
      | otherwise -> rotateUnbalanced $ Node k l r' (height r' - height l)
      where 
        r' = insert key r
        l' = insert key l

rotateUnbalanced :: AVLTree -> AVLTree
rotateUnbalanced t@(Node k l r bf)
  {-left left-}
  | bf == -2 && (bFactor l) == -1 = rotateRight t
  {-left right-}
  | bf == -2 && (bFactor l) == 1 = rotateRight $ Node k (rotateLeft l) r bf
  {-right right-}
  | bf == 2 && (bFactor r) == 1 = rotateLeft t
  {-right left-}
  | bf == 2 && (bFactor r) == -1 = rotateLeft $ Node k l (rotateRight r) bf
  | otherwise = t
rotateUnbalanced Empty = Empty

{-
        z         y
       /         / \
      y    ---> x   z
     / \           /
    x   t3       t3
-}
rotateRight :: AVLTree -> AVLTree
rotateRight (Node z l r _) = 
  Node y x z' (height z' - height x)
  where 
    x = left l
    y = key l
    t3 = right l
    z' = Node z t3 r (height r - height t3)
rotateRight Empty = Empty  

{-
    x             y
     \           / \
      y    ---> x   z
     / \         \ 
   t2   z         t2
-}
rotateLeft :: AVLTree -> AVLTree
rotateLeft (Node x l r _) =
  Node y x' z (height z - height x')
  where 
    y = key r
    z = right r
    t2 = left r
    x' = Node x l t2 (height t2 - height l)
rotateLeft Empty = Empty 

height :: AVLTree -> Int
height Empty = -1
height Node {left = l, right = r} = 1 + maximum [height l, height r]

inOrder :: AVLTree -> T.Text
inOrder Empty = ""
inOrder (Node x l r _) = (inOrder l) <> T.pack (show x) <> " " <> (inOrder r) 

isAVL :: AVLTree -> Bool
isAVL Empty = True
isAVL (Node _ l r bf)  = isAVL l && bf >= -1 || bf <= 1 && isAVL r
