
import java.util.*;

//References : 
//http://www.geeksforgeeks.org/red-black-tree-set-2-insert/
//http://cs.lmu.edu/~ray/notes/redblacktrees/
public class RBTree{

  private class Node{
    Integer key;
    Boolean red; 
    Optional<Node> left = Optional.empty(), right = Optional.empty();

    public Node(Integer key, Boolean red){ this.key = key; this.red = red; }
  }

  private class Rel{
    Optional<Node> parent;
    Optional<Node> node;
    Optional<Boolean> isLeft;

    public Rel(Optional<Node> parent, Optional<Node> node, 
               Optional<Boolean> isLeft){ 
      this.parent = parent; this.node = node; this.isLeft = isLeft; 
    }
  }

  private Optional<Node> root = Optional.empty();

  public void insert(Integer key){
    Optional<Node> n = Optional.of(new Node(key, Boolean.TRUE));
    if( !this.root.isPresent() ){
      n.get().red = Boolean.FALSE;
      this.root = n;
    }else{
      Optional<Node> cur = this.root;
      // This stack could be replaced by the stack of a recursive method, 
      // this is just to show the iterative form.
      Stack<Rel> nodes = new Stack<Rel>();
      nodes.push(new Rel(Optional.empty(), cur, Optional.of(Boolean.TRUE)));
      while( cur.isPresent() ){
        if( key < cur.get().key )
          if( cur.get().left.isPresent() ){
            nodes.push(new Rel(cur, cur.get().left, Optional.of(Boolean.TRUE)));
            cur = cur.get().left;
          }else{ 
            cur.get().left = n; 
            nodes.push(new Rel(cur, cur.get().left, Optional.of(Boolean.TRUE)));
            break; 
          }
        else if( key > cur.get().key )
          if( cur.get().right.isPresent() ){
            nodes.push(new Rel(cur, cur.get().right, Optional.of(Boolean.FALSE)));
            cur = cur.get().right;
          }else{ 
            cur.get().right = n; 
            nodes.push(new Rel(cur, cur.get().right, Optional.of(Boolean.FALSE)));
            break; 
          }
        else
          return;
      }
      this.checkForViolations(nodes);
    }
  }

  private void checkForViolations(Stack<Rel> nodes){
    while(!nodes.empty()){
      Rel r = nodes.pop();
      //If violation occurred
      if(r.node.get().red && r.parent.isPresent() && r.parent.get().red){
        Rel rParent= nodes.pop(); 
        Boolean isChildLeft = r.isLeft.get();
        //Null or black uncle
        if(hasBlackBrother(rParent)){
          Boolean isParentLeft = rParent.isLeft.get();
          Rel rGrandParent= nodes.pop(); 
          //left left
          if(isParentLeft && isChildLeft){
            rotateRight(rGrandParent);
            rGrandParent.node.get().red = Boolean.TRUE;
            rParent.node.get().red = Boolean.FALSE;
          }
          //left right
          if(isParentLeft && !isChildLeft){
            Node newParent = rParent.node.get().right.get();
            rotateLeft(rParent);
            rotateRight(rGrandParent);
            rGrandParent.node.get().red = Boolean.TRUE;
            newParent.red = Boolean.FALSE;
          }
          //right right
          if(!isParentLeft && !isChildLeft){
            rotateLeft(rGrandParent);
            rGrandParent.node.get().red = Boolean.TRUE;
            rParent.node.get().red = Boolean.FALSE;
          }
          //right left
          if(!isParentLeft && isChildLeft){
            Node newParent = rParent.node.get().left.get();
            rotateRight(rParent);
            rotateLeft(rGrandParent);
            rGrandParent.node.get().red = Boolean.TRUE;
            newParent.red = Boolean.FALSE;
          }
        //Red uncle
        }else{
          Node grandParent = rParent.parent.get();
          grandParent.left.get().red = Boolean.FALSE;
          grandParent.right.get().red = Boolean.FALSE;
          if(this.root.get().key != grandParent.key)
            grandParent.red = Boolean.TRUE;
        }
      }
    }
  }

  private Boolean hasBlackBrother(Rel parent){
    if(parent.isLeft.get())
      return !parent.parent.get().right.isPresent() || !parent.parent.get().right.get().red;
    else
      return !parent.parent.get().left.isPresent() || !parent.parent.get().left.get().red;
  }

  //
  //      z         y
  //     /         / \
  //    y    ---> x   z
  //   /
  //  x
  private void rotateRight(Rel r){
    Optional<Node> z = r.node;
    Optional<Node> y = z.get().left;
    Optional<Node> t3 = y.get().right;

    y.get().right = z;
    z.get().left = t3;

    if(r.parent.isPresent()){
      if(r.isLeft.get())
        r.parent.get().left = y;
      else
        r.parent.get().right = y;
    } else
      this.root = y;
  }

  //
  //  x             y
  //   \           / \
  //    y    ---> x   z
  //     \ 
  //      z 
  private void rotateLeft(Rel r){
    Optional<Node> x = r.node;
    Optional<Node> y = x.get().right;
    Optional<Node> t2 = y.get().left;

    y.get().left = x;
    x.get().right = t2;

    if(r.parent.isPresent()){
      if(r.isLeft.get())
        r.parent.get().left = y;
      else
        r.parent.get().right = y;
    } else
      this.root = y;

  }

  //Will return -1 if is an invalid rbtree
  private Integer blackHeight(Optional<Node> n){
    if(!n.isPresent())
      return 0;
    Integer leftBHeight = blackHeight(n.get().left);
    if(leftBHeight == -1)
      return -1;
    Integer rightBHeight = blackHeight(n.get().right);
    if(rightBHeight == -1)
      return -1;
    if(leftBHeight != rightBHeight)
      return -1;
    else
      return leftBHeight + (n.get().red?0:1);
  }

  public Integer blackHeight(){
      return blackHeight(this.root);
  }

  private String inOrder(Optional<Node> n){
    if(n.isPresent()){
      return inOrder(n.get().left) + n.get().key + ":" + (n.get().red?"red ":"black ") + inOrder(n.get().right);
    } else return "";
  }

  @Override
  public String toString(){
    return inOrder(this.root).trim();
  }
}
