import java.util.function.*;
import java.util.*;

public class WeightedGraph{

  private Integer numVertices;
  //We'll have Integer labeled vertices for easier identification of these on the adjacencyMatrix,
  //row 4 corresponds to label 4 and so on
  private Integer[] vertices;
  //We'll assume -1 means no connection
  private Integer[][] adjacencyMatrix;

  public WeightedGraph(Integer nVertices){
    this.numVertices = nVertices;
    this.vertices = new Integer[this.numVertices];
    for(int i = 0; i < this.numVertices; i++)
      this.vertices[i] = i;
    this.adjacencyMatrix = new Integer[this.numVertices][this.numVertices];
    for(int i = 0; i < this.numVertices; i++)
      for(int j = 0; j < this.numVertices; j++)
        this.adjacencyMatrix[i][j] = Integer.MAX_VALUE;
  }

  // TODO Validate that both vertices exist
  public void addEdge(Integer v1, Integer v2, Integer cost){
    this.adjacencyMatrix[v1][v2] = cost;
    this.adjacencyMatrix[v2][v1] = cost;
  }

  public void addDirectedEdge(Integer v1, Integer v2, Integer cost){
    this.adjacencyMatrix[v1][v2] = cost;
  }

  //Using Prim's algorithm
  public String minimumSpanningTree(){
    class Edge{
      Integer source;
      Integer destination;
      Integer cost;
      public Edge(Integer s, Integer d, Integer c){ this.source = s; this.destination = d; this.cost = c; }
    }
    PriorityQueue<Edge> queue = new PriorityQueue<Edge>((e1, e2) -> { return e1.cost - e2.cost; });
    Boolean[] visitedVertices = new Boolean[this.numVertices];
    for(int i = 0; i < this.numVertices; i++)
      visitedVertices[i] = false;
    String visitOrder = "*";
    Integer numVisitedVertices = 0;

    //Visit first vertex
    Integer currentVertex = 0;
    visitedVertices[currentVertex] = true;
    numVisitedVertices++;

    // We assume the graph is connected
    while(numVisitedVertices < numVertices){
      for(int i = 0; i < this.numVertices; i++){
        Integer cost = this.adjacencyMatrix[currentVertex][i];
        if(cost > -1)
          queue.add(new Edge(currentVertex, i, cost));
      }
      // TODO we're discarding here based on the visited edges, ideally the queue should be updated internally
      Edge nextEdge;
      do{
        nextEdge = queue.poll();
      }while(visitedVertices[nextEdge.destination]);
      visitOrder += " " + nextEdge.source + "-" + nextEdge.destination;
      currentVertex = nextEdge.destination;
      visitedVertices[currentVertex] = true;
      numVisitedVertices++;
    }

    return visitOrder;
  }

  //could be further improved with a PriorityQueue see https://www.geeksforgeeks.org/dijkstras-shortest-path-algorithm-using-priority_queue-stl/
  //Also the parent node could be saved in the resulting array
  public Integer[] dijkstraShortestPath(Integer source){
    Integer[] result = new Integer[this.numVertices];
    Boolean[] visitedVertices = new Boolean[this.numVertices];
    for(int i = 0; i < this.numVertices; i++){
      visitedVertices[i] = false;
      result[i] = Integer.MAX_VALUE;
    }
    Integer numVisitedVertices = 0;

    //Visit first vertex
    Integer currentVertex = source;
    // Visiting vertex to itself is 0
    result[currentVertex] = 0;

    // We assume the graph is connected
    while(numVisitedVertices < this.numVertices){

      for(int i = 0; i < this.numVertices; i++){
        // cost from start to vertex
        Integer totalCost = result[currentVertex] + this.adjacencyMatrix[currentVertex][i];
        if(!visitedVertices[i] &&
            totalCost > 0 && // Avoid integer overflow
            totalCost < result[i])
          result[i] = totalCost;
      }

      Supplier<Optional<Integer>> getMin = () -> {
        Integer index = -1;
        Integer min = Integer.MAX_VALUE;
        for(int i = 0; i < this.numVertices; i++){
          if(!visitedVertices[i] &&
              result[i] < min){
            min = result[i];
            index = i;
          }
        }
        if(index == -1)
          return Optional.empty();
        else
          return Optional.of(index);
      };

      visitedVertices[currentVertex] = true;
      numVisitedVertices++;

      Optional<Integer> min = getMin.get();
      if(!min.isPresent())
        break;
      else
        currentVertex = min.get();
    }

    return result;
  }

  public Integer[][] floydWarshall(){
    Integer[][] result = this.adjacencyMatrix.clone();
    for(int row = 0; row < this.numVertices; row++)
      for(int column = 0; column < this.numVertices; column++)
        if(result[row][column] != Integer.MAX_VALUE)
          for(int candidateRow = 0; candidateRow < this.numVertices; candidateRow++){
            if(result[candidateRow][row] == Integer.MAX_VALUE)
              continue;
            Integer cost = result[row][column] + result[candidateRow][row];
            if(result[candidateRow][column] > cost)
              result[candidateRow][column] = cost;
          }
    return result;
  }
}
