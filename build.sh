
## Run haskell
## From https://gist.github.com/kuznero/e1f6e53a2ec386a45e579f63b45f53db
# nix-shell -p "haskell.packages.ghc821.ghcWithPackages (pkgs: with pkgs; [ text hspec ])" --run "runhaskell BSTreeSpec"
# nix-shell -p "haskellPackages.ghcWithPackages (pkgs: with pkgs; [ text hspec ])" --run "runhaskell BSTreeSpec"

## Run all
javac WeightedGraph.java &&\
javac -cp .:junit-4.12.jar:oleaster-runner-0.1.2.jar WeightedGraphTest.java &&\
java -cp .:junit-4.12.jar:oleaster-runner-0.1.2.jar:hamcrest-core-1.3.jar org.junit.runner.JUnitCore WeightedGraphTest

#javac DirectedGraph.java &&\
#javac -cp .:junit-4.12.jar:oleaster-runner-0.1.2.jar DirectedGraphTest.java &&\
#java -cp .:junit-4.12.jar:oleaster-runner-0.1.2.jar:hamcrest-core-1.3.jar org.junit.runner.JUnitCore DirectedGraphTest

#javac Graph.java &&\
#javac -cp .:junit-4.12.jar:oleaster-runner-0.1.2.jar GraphTest.java &&\
#java -cp .:junit-4.12.jar:oleaster-runner-0.1.2.jar:hamcrest-core-1.3.jar org.junit.runner.JUnitCore GraphTest

## Test
#javac -cp .:junit-4.12.jar:oleaster-runner-0.1.2.jar BSTreeTest.java

## Run Test
#java -cp .:junit-4.12.jar:oleaster-runner-0.1.2.jar:hamcrest-core-1.3.jar org.junit.runner.JUnitCore BSTreeTest

## Source
#javac AVLTree.java

## Test
#javac -cp .:junit-4.12.jar:oleaster-runner-0.1.2.jar AVLTreeTest.java

## Run Test
#java -cp .:junit-4.12.jar:oleaster-runner-0.1.2.jar:hamcrest-core-1.3.jar org.junit.runner.JUnitCore AVLTreeTest

## Source
#javac RBTree.java

## Test
#javac -cp .:junit-4.12.jar:oleaster-runner-0.1.2.jar RBTreeTest.java

## Run Test
#java -cp .:junit-4.12.jar:oleaster-runner-0.1.2.jar:hamcrest-core-1.3.jar org.junit.runner.JUnitCore RBTreeTest


