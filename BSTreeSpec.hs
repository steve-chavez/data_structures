{-# LANGUAGE OverloadedStrings #-}

import Test.Hspec
import BSTree

main :: IO ()
main = hspec $ do
  describe "BSTree" $ do

    let testTree = insert 1 $ insert 3 $ insert 4 $ insert 5 $ insert 9 $ 
               insert 10 Empty

    it "should insert keys" $ 
      inOrder testTree `shouldBe` "1 3 4 5 9 10 "

    it "shouldn't delete if key is not present" $ 
      inOrder (delete 100 $ delete 12 testTree) `shouldBe` "1 3 4 5 9 10 "

    it "should find key if it's present" $ 
      find 3 testTree `shouldBe` Just 3 

    it "shouldn't find key if it's not present" $ 
      find 8 testTree `shouldBe` Nothing

    describe "root node deletion" $ do
      it "should work for just root node" $ do
        let t = insert 3 Empty
        inOrder (delete 3 t) `shouldBe` ""

      it "should work for root node with a leaf" $ do
        let t = insert 5 $ insert 3 Empty
        inOrder (delete 3 t) `shouldBe` "5 "
        let t1 = insert 1 $ insert 3 Empty
        inOrder (delete 3 t1) `shouldBe` "1 "

      it "should work for root node with two leaves" $ do
        let t = insert 1 $ insert 5 $ insert 3 Empty
        inOrder (delete 3 t) `shouldBe` "1 5 "

    let testTree1 = insert 74 $ insert 92 $ insert 78 $ insert 43 $ insert 60 $ 
                    insert 41 $ insert 90 $ insert 55 $ insert 72 $ insert 26 $ 
                    insert 38 Empty

    describe "non-root node deletion" $ do
      it "should work for a leaf" $ do
        inOrder (delete 43 testTree1) `shouldBe` "26 38 41 55 60 72 74 78 90 92 "
        inOrder (delete 74 testTree1) `shouldBe` "26 38 41 43 55 60 72 78 90 92 "
        inOrder (delete 92 testTree1) `shouldBe` "26 38 41 43 55 60 72 74 78 90 "
        inOrder (delete 26 testTree1) `shouldBe` "38 41 43 55 60 72 74 78 90 92 "

      describe "of a node with one child" $ do
        it "should work for a left" $ 
          inOrder (delete 78 testTree1) `shouldBe` "26 38 41 43 55 60 72 74 90 92 "

        it "should work for a right" $ 
          inOrder (delete 41 testTree1) `shouldBe` "26 38 43 55 60 72 74 78 90 92 "

      describe "of a node with two childrens" $ do
        it "should work when the succesor has no right child" $ 
          inOrder (delete 72 testTree1) `shouldBe` "26 38 41 43 55 60 74 78 90 92 "

        it "should work when the succesor has a right child" $ do
          inOrder (delete 38 testTree1) `shouldBe` "26 41 43 55 60 72 74 78 90 92 "

    it "should calculate the correct height" $
      height testTree1 `shouldBe` 4

    it "should calculate the correct width" $
      width testTree1 `shouldBe` 4

    it "should invert the tree" $ do
      let tree = insert 9 $ insert 6 $ insert 7 $ insert 3 $ insert 1 $ 
                 insert 2 $ insert 4 Empty
      inOrder tree `shouldBe` "1 2 3 4 6 7 9 "
      inOrder (invert tree) `shouldBe` "9 7 6 4 3 2 1 "
      inOrder (invert (invert tree)) `shouldBe` "1 2 3 4 6 7 9 "

