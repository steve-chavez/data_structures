{-# LANGUAGE OverloadedStrings #-}

module RBTree( 
  RBTree(..), insert, blackHeight, isValidRBTree, inOrder
) where

import Data.Monoid 
import qualified Data.Text as T(Text, pack)
import Prelude

data Color = Red | Black deriving (Eq)
instance Show Color where
  show Red  = "red"
  show Black = "black"

data RBTree = 
  Empty | Node { 
    key     :: Int, 
    color   :: Color,
    left    :: RBTree, 
    right   :: RBTree
  } deriving (Eq, Show)

insert :: Int -> RBTree -> RBTree
insert k t = ensureRootIsBlack $ insert' k t
  where
    ensureRootIsBlack (Node k _ l r) = Node k Black l r

insert' :: Int -> RBTree -> RBTree
insert' k Empty = Node k Red Empty Empty
insert' k (Node x c l r) 
  | k < x = fixViolation $ Node x c (insert' k l) r
  | otherwise = fixViolation $ Node x c l (insert' k r)

fixViolation :: RBTree -> RBTree
{- Red uncle left left
 
        B-g             R-g
     R-p   R-u  ->   B-p   B-u
  R-x             R-x

-}
fixViolation (Node g Black (Node p Red x@Node{color=Red} pr) (Node u Red ul ur)) =
  Node g Red (Node p Black x pr) (Node u Black ul ur)
{- Red uncle left right
 
         B-g             R-g
     R-p     R-u  -> B-p     B-u
        R-x             R-x

-}
fixViolation (Node g Black (Node p Red pl x@Node{color=Red}) (Node u Red ul ur)) =
  Node g Red (Node p Black pl x) (Node u Black ul ur)
{- Red uncle right right
 
      B-g              R-g
   R-u   R-p    ->  B-u   B-p
            R-x              R-x

-}
fixViolation (Node g Black (Node u Red ul ur) (Node p Red pl x@Node{color=Red})) = 
  Node g Red (Node u Black ul ur) (Node p Black pl x) 
{- Red uncle right left
 
       B-g                R-g
   R-u     R-p    ->  B-u     B-p
        R-x                R-x

-}
fixViolation (Node g Black (Node u Red ul ur) (Node p Red x@Node{color=Red} pr)) = 
  Node g Red (Node u Black ul ur) (Node p Black x pr) 
{- Black or null uncle left left
 
           B-g               B-p
       R-p     B-u  ->   R-x     R-g
    R-x   t3 t4   t5   t1   t2 t3   B-u
  t1   t2                         t4   t5

-}
fixViolation (Node g Black (Node p Red x@Node{color=Red} t3) u@Node{color=Black}) = 
  Node p Black x (Node g Red t3 u)
fixViolation (Node g Black (Node p Red x@Node{color=Red} t3) Empty) = 
  Node p Black x (Node g Red t3 Empty)
{- Black or null uncle left right
 
             B-g                 B-x
       R-p        B-u   ->   R-p      R-g
     t1   R-x   t4   t5    t1   t2  t3   B-u
        t2   t3                        t4   t5

-}
fixViolation (Node g Black (Node p Red t1 (Node x Red t2 t3)) u@Node{color=Black}) = 
  Node x Black (Node p Red t1 t2) (Node g Red t3 u)
fixViolation (Node g Black (Node p Red t1 (Node x Red t2 t3)) Empty) = 
  Node x Black (Node p Red t1 t2) (Node g Red t3 Empty)
{- Black or null uncle right right
 
            B-g                        B-p
       B-u       R-p     ->       R-g       R-x
     t1   t2  t3    R-x        B-u   t3   t4   t5
                  t4   t5    t1   t2

-}
fixViolation (Node g Black u@Node{color=Black} (Node p Red t3 x@Node{color=Red})) = 
  Node p Black (Node g Red u t3) x
fixViolation (Node g Black Empty (Node p Red t3 x@Node{color=Red})) = 
  Node p Black (Node g Red Empty t3) x
{- Black or null uncle right left
 
            B-g                        B-x
       B-u        R-p     ->       R-g       R-p
     t1   t2   R-x   t5        B-u    t3   t4   t5
             t3   t4         t1   t2   

-}
fixViolation (Node g Black Empty (Node p Red (Node x Red t3 t4) t5)) = 
  Node x Black (Node g Red Empty t3) (Node p Red t4 t5)
fixViolation t = t

blackHeight :: RBTree -> Integer
blackHeight Empty = 0
blackHeight Node{color=Black, left=l} = 1 + blackHeight l
blackHeight Node{color=Red, left=l} = 0 + blackHeight l

isValidRBTree :: RBTree -> Bool
isValidRBTree Empty = False
isValidRBTree Node{left=l, right=r} = blackHeight l == blackHeight r

inOrder :: RBTree -> T.Text
inOrder Empty = ""
inOrder (Node x c l r) = (inOrder l) <> T.pack (show x) <> ":" <> T.pack (show c) 
                          <> " " <>(inOrder r) 
