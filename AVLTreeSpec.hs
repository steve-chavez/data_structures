{-# LANGUAGE OverloadedStrings #-}

import Test.Hspec
import AVLTree
import System.Random

main :: IO ()
main = hspec $ do
  describe "AVLTree" $ do

    it "should insert keys and maintain AVL property" $ do
      let tree = insert 74 $ insert 92 $ insert 78 $ insert 43 $ 
                     insert 60 $ insert 41 $ insert 90 $ insert 55 $ 
                     insert 72 $ insert 26 $ insert 38 Empty
      inOrder tree `shouldBe` "26 38 41 43 55 60 72 74 78 90 92 "
      tree `shouldSatisfy` isAVL

    it "should insert 100 random keys and maintain AVL property" $ do
      let nums = sequence $ replicate 100 $ randomRIO (1,100)
      tree <- (foldr insert Empty <$> nums) 
      tree `shouldSatisfy` isAVL
