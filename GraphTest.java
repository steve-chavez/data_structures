
import static com.mscharhag.oleaster.runner.StaticRunnerSupport.*;
import com.mscharhag.oleaster.runner.OleasterRunner;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;
import java.util.Random;

@RunWith(OleasterRunner.class)
public class GraphTest {{
  describe("Graph", () -> {

    //  1 - 5 - 7
    //  |
    //  |  2
    //  | /
    //  0 - 3 - 6 - 8
    //  |
    //  4
    Graph g1 = new Graph(9);
    g1.addEdge(0, 1); g1.addEdge(1, 5); g1.addEdge(5, 7);
    g1.addEdge(0, 2);
    g1.addEdge(0, 3); g1.addEdge(3, 6); g1.addEdge(6, 8);
    g1.addEdge(0, 4);

    //   0
    //  / \
    // 1   2
    // | \ |
    // 4 - 5
    //  \ /
    //   6
    Graph g2 = new Graph(7);
    g2.addEdge(0, 1); g2.addEdge(0, 2);
    g2.addEdge(1, 4); g2.addEdge(1, 5);
    g2.addEdge(2, 5);
    g2.addEdge(4, 5); g2.addEdge(4, 6);
    g2.addEdge(5, 6);

    it("should do a DFS", () -> {
      assertEquals(
        g1.depthFirstTraversal(),
        "*-0-1-5-7-2-3-6-8-4");
      assertEquals(
        g2.depthFirstTraversal(),
        "*-0-1-4-5-2-6");
    });

    it("should do a BFS", () -> {
      assertEquals(
        g1.breadthFirstTraversal(),
        "*-0-1-2-3-4-5-6-7-8");
      assertEquals(
        g2.breadthFirstTraversal(),
        "*-0-1-2-4-5-6");
    });

    it("should show the minimum spanning tree", () -> {
      assertEquals(
        g1.miniumSpanningTree(),
        "* 0-1 1-5 5-7 0-2 0-3 3-6 6-8 0-4");
      assertEquals(
        g2.miniumSpanningTree(),
        "* 0-1 1-4 4-5 5-2 5-6");
    });
  });
}}
