{-# LANGUAGE OverloadedStrings #-}

import Test.Hspec
import RBTree
import System.Random

main :: IO ()
main = hspec $ do
  describe "RBTree" $ do

    context "red uncle case" $ do
      it "should handle left left subcase" $ do
        let tree = insert 1 $ insert 5 $ insert 20 $ insert 10 $ Empty
        inOrder tree `shouldBe` "1:red 5:black 10:black 20:black "
        blackHeight tree `shouldBe` 2 

      it "should handle left right subcase" $ do
        let tree = insert 6 $ insert 5 $ insert 20 $ insert 10 $ Empty
        inOrder tree `shouldBe` "5:black 6:red 10:black 20:black "
        blackHeight tree `shouldBe` 2 

      it "should handle right right subcase" $ do
        let tree = insert 24 $ insert 5 $ insert 20 $ insert 10 $ Empty
        inOrder tree `shouldBe` "5:black 10:black 20:black 24:red "
        blackHeight tree `shouldBe` 2 

      it "should handle right left subcase" $ do
        let tree = insert 19 $ insert 5 $ insert 20 $ insert 10 $ Empty
        inOrder tree `shouldBe` "5:black 10:black 19:red 20:black "
        blackHeight tree `shouldBe` 2 

    context "black uncle case" $ do
      it "should handle left left subcase" $ do
        let tree = insert 1 $ insert 5 $ insert 10 $ Empty
        inOrder tree `shouldBe` "1:red 5:black 10:red "
        blackHeight tree `shouldBe` 1 

      it "should handle right right subcase" $ do
        let tree = insert 30 $ insert 20 $ insert 10 $ Empty
        inOrder tree `shouldBe` "10:red 20:black 30:red "
        blackHeight tree `shouldBe` 1 

      it "should handle left right subcase" $ do
        let tree = insert 6 $ insert 5 $ insert 10 $ Empty
        inOrder tree `shouldBe` "5:red 6:black 10:red "
        blackHeight tree `shouldBe` 1 

      it "should handle right left subcase" $ do
        let tree = insert 15 $ insert 20 $ insert 10 $ Empty
        inOrder tree `shouldBe` "10:red 15:black 20:red "
        blackHeight tree `shouldBe` 1 

    it "should handle more complex cases" $ do
      let tree = insert 7 $ insert 6 $ insert 3 $ insert 9 $ 
                 insert 5 $ insert 4 $ insert 1 $ insert 2 $ Empty
      inOrder tree `shouldBe` "1:black 2:black 3:red 4:black 5:red 6:red 7:black 9:red "
      blackHeight tree `shouldBe` 2 

      let tree2 = insert 11 $ insert 10 $ insert 9 $ insert 8 $ insert 7 $ 
                  insert 6 $ insert 5 $ insert 4 $ insert 3 $ insert 2 $ 
                  insert 1 $ Empty
      inOrder tree2 `shouldBe` "1:black 2:black 3:black 4:black 5:black 6:black 7:black 8:red 9:red 10:black 11:red "
      blackHeight tree2 `shouldBe` 3 

    it "should insert 100 random keys and maintain red black property" $ do
      let nums = sequence $ replicate 100 $ randomRIO (1,100)
      tree <- (foldr insert Empty <$> nums) 
      tree `shouldSatisfy` isValidRBTree
